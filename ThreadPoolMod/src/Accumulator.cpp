//
// Created by user on 19-7-16.
//
#include <future>
#include <algorithm>
#include <thread>
#include <type_traits>
#include <cmath>
#include <numeric>
#include <iostream>
#include <cstdio>
#include <cstdlib>

#include "../include/ThreadPoolMod/ThreadPool.h"
#include "../include/ThreadPoolMod/Accumulator.h"

accumulateBlockLikelihood::accumulateBlockLikelihood(double *start_, size_t nCol_, double *parameter_, size_t nPara_, unsigned long int linesToAcc_)
        :start{start_}, nCol{nCol_},parameter{parameter_}, nPara{nPara_}, linesToAcc{linesToAcc_}{}

double accumulateBlockLikelihood::operator()() {
//    std::cout << "Is this called?" <<std::endl;
    double sum = 0.0;
//    std::cout << numberToAccum << std::endl;
    for (int i = 0; i < linesToAcc ; i++){
//        std::cout << i << std::endl;
        sum += start[i*nCol];

    }
    return sum;
}


accumulateBlockDerivative::accumulateBlockDerivative(double* vecPartial_, double * start_, size_t nCol_, double * parameter_, size_t nPara_, unsigned long linesToAcc_):
vecPartial{vecPartial_},start{start_},nCol{nCol_}, parameter{parameter_}, nPara{nPara_}, linesToAcc{linesToAcc_}{ }


int accumulateBlockDerivative::operator()(){
//    To be finished
    return 1;
}

accumulator::accumulator(int numThreads_, int nPara_):numThreads{numThreads_}, pool{numThreads_-1} {
    matPartial = (double *) malloc(sizeof(double)*(numThreads_-1)*nPara_);
}

accumulator::~accumulator(){
    free(matPartial);
}






double accumulator::accumulateLikelihood(double * data, long nRow, long nCol,  double * parameter, long nPara, long stride){
    std::vector<std::future<double> > futures(numThreads-1);

    for(unsigned long i=0;i<(numThreads-1);++i)
    {
        double * start = &data[i*stride*nCol];
        accumulateBlockLikelihood chunk(start, nCol, parameter, nPara, stride);
        futures[i]=pool.submit(chunk);

    }
    accumulateBlockLikelihood last_chunk(&data[(numThreads-1)*stride*nCol], nCol,  parameter, nPara, nRow - (numThreads-1)*stride);
    double last_result=last_chunk();
    double result=0.0;
    for(unsigned long i=0;i<(numThreads-1);++i)
    {
        double temp = futures[i].get();
        result+=temp;
//        std::cout << temp << std::endl;

    }
    result+= last_result;
    return result;
}

void accumulator::accumulateDerivatives(double *out, double *data, long nRow, long nCol, double *parameter, long nPara,
                                        long stride) {
    std::vector<std::future<int> > futures(numThreads-1);

    for(unsigned long i=0;i<(numThreads-1);++i)
    {
        double * start = &data[i*stride*nCol];
        accumulateBlockDerivative chunk(&matPartial[i*nPara], start, nCol, parameter, nPara, stride);
        futures[i]=pool.submit(chunk);

    }
    accumulateBlockDerivative last_chunk(&matPartial[(numThreads-1)*nPara],&data[(numThreads-1)*stride*nCol], nCol,  parameter, nPara, nRow - (numThreads-1)*stride);
    double last_result=last_chunk();
    int result=0.0;
    for(unsigned long i=0;i<(numThreads-1);++i)
    {
        int temp = futures[i].get();
        int tempIndex = i*nPara;
        for (unsigned long j =0; j<nPara;j++){
            out[j] += matPartial[tempIndex+j];
        }
    }
    int tempIndex = (numThreads-1)*nPara;
    for(unsigned long i=0;i<nPara;i++){
        out[i]+=matPartial[tempIndex+i];
    }

}