//
// Created by user on 19-7-16.
//
#include "../include/ThreadPoolMod/ThreadJoin.h"
#include <thread>
#include <vector>

join_thread::join_thread(std::vector<std::thread>& other):threads(other){}

join_thread::~join_thread() {
    for(unsigned long i=0;i<threads.size();++i)
    {
        if(threads[i].joinable())
            threads[i].join();
    }
}