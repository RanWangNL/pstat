//
// Created by user on 19-7-16.
//

#ifndef CUDATRIAL_ACCUMULATOR_H
#define CUDATRIAL_ACCUMULATOR_H
//This function take index as the input data
//parameter as the parameter data
//NumberToAccum as the number of rows that each thread is supposed to add
#include "ThreadPool.h"

class accumulateBlockLikelihood {
public:
    double *start;
    size_t nCol;
    double *parameter;
    size_t nPara;
    unsigned long int linesToAcc;
//    start -> the exact index (not the lines) where the accumulation begins
//    nCol -> the number of columns in the data set
//    parameter -> parameter
//    nPara -> size of parameter
//    linesToAcc -> number of lines (not entries) that each block is supposed to accumulate
    accumulateBlockLikelihood(double *start_, size_t nCol_, double *parameter_, size_t nPara_, unsigned long int linesToAcc_);

    double operator()();
};

class accumulateBlockDerivative{
public:
    double *vecPartial;

    double *start;
    size_t nCol;
    double * parameter;
    size_t nPara;

//    vecPartial -> the address of where to store the partial sums of vector of derivatives
//    Assuming that the size of vecPartials is the same as the number of parameters
    unsigned long int linesToAcc;
    accumulateBlockDerivative(double*, double *, size_t, double*, size_t,  unsigned long int);

    int operator()();
};



//Return the accumulate sum of likelihood using an existing thread pool
//data -> the pointer to where the data is stored
//parameter -> the pointer to where the current parameter is stored
//stride -> number of lines that a thread should accumulate
//Requirement:Stride must be smaller than nRow

class accumulator{
public:
    int numThreads;
//    The number of total threads ( = number of threads in pool + 1)
    threadpool pool;
    double* matPartial;
//    matPartial -> a numThreads * nPara sized temporary array to hold the data
    accumulator(int numThreads_, int nPara_);
    ~accumulator();
    double accumulateLikelihood(double * data, long nRow, long nCol,  double * parameter, long nPara, long stride);

    void accumulateDerivatives(double * out, double * data, long nRow, long nCol, double * parameter, long nPara, long stride);
};

#endif //CUDATRIAL_ACCUMULATOR_H
