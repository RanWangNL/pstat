
#include <iostream>
#include "cuda_runtime.h"

#define PARTIALSUM(sPartials, tid, nPara, offset){\
                    for(int i = 0;i<nPara;i++){\
                        sPartials[tid*nPara+i] = sPartials[(tid+offset)*nPara+i];\
                    }\
                }

template<unsigned int numThreads>
__global__ void reduceFuncVector(double * out, const double *in, const double *parameter,  size_t nRow, size_t nCol, size_t nPara) {
    extern __shared__ double sPartials[];
    const unsigned int tid = threadIdx.x;
    const unsigned int offset = numThreads * blockIdx.x;
    double * sum = new double[nPara];
    for (size_t i = tid; i < nRow; i += numThreads * blockDim.x) {
        sum[0] += in[(i + offset) * nCol]; //This needs to be changed later on
    }
    for (int i=0;i<nPara;i++){
        sPartials[tid*nPara+i] = sum[i];
    }
    free(sum);
    //This needs to be changed
    __syncthreads();
//    printf("This is %d rows, with values equals %f \n", tid, in[(tid+offset)*nCol]);
    if (numThreads >= 1024) {
        if (tid < 512) {
            PARTIALSUM(sPartials, tid, nPara, 512);
        }
        __syncthreads();
    }
    if (numThreads >= 512) {
        if (tid < 256) {
            PARTIALSUM(sPartials, tid, nPara, 256);
        }
        __syncthreads();
    }
    if (numThreads >= 256) {
        if (tid < 128) {
            PARTIALSUM(sPartials, tid, nPara, 128);
        }
        __syncthreads();
    }
    if (numThreads >= 128) {
        if (tid < 64) {
            PARTIALSUM(sPartials, tid, nPara, 64);
        }
        __syncthreads();
        // warp synchronous at the end
        if (tid < 32) {
//            volatile double *wsSum = sPartials;
            if (numThreads >= 64) { PARTIALSUM(sPartials, tid, nPara, 32); }
            if (numThreads >= 32) { PARTIALSUM(sPartials, tid, nPara, 16); }
            if (numThreads >= 16) { PARTIALSUM(sPartials, tid, nPara, 8); }
            if (numThreads >= 8) { PARTIALSUM(sPartials, tid, nPara, 4);}
            if (numThreads >= 4) { PARTIALSUM(sPartials, tid, nPara, 2); }
            if (numThreads >= 2) { PARTIALSUM(sPartials, tid, nPara, 1); }
            if (tid == 0) {

                out[blockIdx.x] = sPartials[0];
            }
        }
    }
}