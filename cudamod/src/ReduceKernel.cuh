//
// Created by user on 19-7-16.
//

#include "cuda_runtime.h"
#ifndef PSTAT_REDUCEKERNEL_CUH_H
#define PSTAT_REDUCEKERNEL_CUH_H
template<unsigned int numThreads>
__global__ void reduceFunc(double * out, const double *in, const double *parameter,  size_t nRow, size_t nCol,size_t nPara);

template<unsigned int numThreads>
void
        reduceFunc_Template(double *answer, const double *in, const double * parameter, size_t nRow, size_t nCol, size_t nPara, int numBlocks);

void reduceFunc(double *out, const double *in, const double *parameter,  size_t nRow,
                size_t nCol, size_t nPara,  int numBlocks, int numThreads);

__global__ void reduceSum(double *out, const double *in, size_t N );
#endif //CUDATRIAL_REDUCEKERNEL_CUH_H
