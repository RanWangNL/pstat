

//#define __BOUNDCHECK__ 1
#ifndef PSTAT_MACRO_H
#define PSTAT_MACRO_H
#include <cstdlib>
#include <cstdio>

#ifdef __BOUNDCHECK__
#define CHECK_BOUND(i, j) (assert((i)<(j)))
#else
#define CHECK_BOUND(i,j)
#endif

#ifdef __CUDACC__
#define CUDA_CALLABLE_MEMBER __host__ __device__
#else
#define CUDA_CALLABLE_MEMBER
#endif

#ifdef __CUDACC__
#define CUDA_HOST __host__
#else
#define CUDA_HOST
#endif

#ifdef __CUDACC__
#define CUDA_DEVICE __device__
#else
#define CUDA_DEVICE
#endif
//This function replaces the indexes with what is needed
#define __IR__(i,j, nRow,nCol) [(i)*(nCol)+(j)]
#define __IC__(i,j, nRow,nCol) [(i)+(j)*(nRow)]


#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
    if (err != cudaSuccess) {
        printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_FAILURE );
    }
}

#endif