//
// Created by user on 7-8-16.
//

#ifndef PSTAT_CUDAMATRIX_H
#define PSTAT_CUDAMATRIX_H

#include "../../Auxilliary/src/PointerMatrix.h"
#include "Macro.cuh"
struct RowMajor;
struct ColMajor;
struct Mistake;

struct View;
struct NotView;
template<typename T, typename Type, typename ViewORNot>
struct DetermineRowColMajor {
    using type = Mistake;
};

template<typename T>
struct DetermineRowColMajor<T,RowMajor,NotView>{
    using type = PointerMatrixRowMajor<T>;
};

template<typename T>
struct DetermineRowColMajor<T, ColMajor, NotView>{
    using type = PointerMatrixColMajor<T>;
};

template<typename T>
struct DetermineRowColMajor<T, RowMajor, View>{
    using type = PointerMatrixViewRowMajor<T>;
};

template<typename T>
struct DetermineRowColMajor<T, ColMajor, View>{
    using type = PointerMatrixViewColMajor<T>;
};

template<typename T, typename isRowMajor, typename isView>
class CUDAMatrix {
public:
    T * devData;
    size_t nRow;
    size_t nCol;
    using hostDataType = typename DetermineRowColMajor<T, isRowMajor, isView>::type;
    hostDataType hostData;

    CUDA_HOST CUDAMatrix(size_t Row, size_t Col, T * data);
    CUDA_HOST ~CUDAMatrix();

    CUDA_HOST void syncDeviceToHost();
    CUDA_HOST void syncHostToDevice();
};


#endif //PSTAT_CUDAMATRIX_H
