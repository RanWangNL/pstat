// Created by user on 19-7-16.
//

#include "cuda_runtime.h"
#include <iostream>
#include <device_launch_parameters.h>
#include "ReduceKernel.cuh"
#include "Macro.cuh"

template<unsigned int numThreads>
__global__ void reduceFunc(double * out, const double *in, const double *parameter,  size_t nRow, size_t nCol, size_t nPara) {
    extern __shared__ double sPartials[];
    const unsigned int tid = threadIdx.x;
    const unsigned int offset = numThreads * blockIdx.x;
    double sum = 0.0;
    for (size_t i = tid; i < nRow; i += numThreads * blockDim.x) {
        sum += in[(i + offset) * nCol]; //This needs to be changed later on
//        printf("This is %d rows, with values equals %f \n", i, in[(i+offset)*nCol]);
    }
    sPartials[tid] = sum;
    __syncthreads();
//    printf("This is %d rows, with values equals %f \n", tid, in[(tid+offset)*nCol]);
    if (numThreads >= 1024) {
        if (tid < 512) {
            sPartials[tid] += sPartials[tid + 512];
        }
        __syncthreads();
    }
    if (numThreads >= 512) {
        if (tid < 256) {
            sPartials[tid] += sPartials[tid + 256];
        }
        __syncthreads();
    }
    if (numThreads >= 256) {
        if (tid < 128) {
            sPartials[tid] += sPartials[tid + 128];
        }
        __syncthreads();
    }
    if (numThreads >= 128) {
        if (tid < 64) {
            sPartials[tid] += sPartials[tid + 64];
        }
        __syncthreads();
        // warp synchronous at the end
        if (tid < 32) {
            volatile double *wsSum = sPartials;
            if (numThreads >= 64) { wsSum[tid] += wsSum[tid + 32]; }
            if (numThreads >= 32) { wsSum[tid] += wsSum[tid + 16]; }
            if (numThreads >= 16) { wsSum[tid] += wsSum[tid + 8]; }
            if (numThreads >= 8) { wsSum[tid] += wsSum[tid + 4]; }
            if (numThreads >= 4) { wsSum[tid] += wsSum[tid + 2]; }
            if (numThreads >= 2) { wsSum[tid] += wsSum[tid + 1]; }
            if (tid == 0) {
                out[blockIdx.x] = wsSum[0];
            }
        }
    }
}
//    printf("After doing all the adding, I have some sum to be %f from block %d\n", out[blockIdx.x], blockIdx.x);


__global__ void printFirstElement(double * element){
//    element[0] = 0.1;
//    element[1] = 0.2;
    printf("The first element is %f\n", element[0]);
}
template<unsigned int numThreads>
__global__ void
reduceSum(double *out, const double *in, size_t N )
{
    extern __shared__ double sPartials[];
    const unsigned int tid = threadIdx.x;
    double sum =0.0;
    for ( size_t i = blockIdx.x*numThreads + tid;
          i < N;
          i += numThreads*gridDim.x )
    {
        sum += in[i];
    }
    sPartials[tid] = sum;
    __syncthreads();

    if (numThreads >= 1024) {
        if (tid < 512) {
            sPartials[tid] += sPartials[tid + 512];
        }
        __syncthreads();
    }
    if (numThreads >= 512) {
        if (tid < 256) {
            sPartials[tid] += sPartials[tid + 256];
        }
        __syncthreads();
    }
    if (numThreads >= 256) {
        if (tid < 128) {
            sPartials[tid] += sPartials[tid + 128];
        }
        __syncthreads();
    }
    if (numThreads >= 128) {
        if (tid <  64) {
            sPartials[tid] += sPartials[tid +  64];
        }
        __syncthreads();
    }
    // warp synchronous at the end
    if ( tid < 32 ) {
        volatile double *wsSum = sPartials;
        if (numThreads >=  64) { wsSum[tid] += wsSum[tid + 32]; }
        if (numThreads >=  32) { wsSum[tid] += wsSum[tid + 16]; }
        if (numThreads >=  16) { wsSum[tid] += wsSum[tid +  8]; }
        if (numThreads >=   8) { wsSum[tid] += wsSum[tid +  4]; }
        if (numThreads >=   4) { wsSum[tid] += wsSum[tid +  2]; }
        if (numThreads >=   2) { wsSum[tid] += wsSum[tid +  1]; }
        if ( tid == 0 ) {
            out[blockIdx.x] = sPartials[0];
            printf("Result is %f\n", out[blockIdx.x]);
        }
    }
}

template<unsigned int numThreads> void reduceFunc_Template(double *answer, const double *in, const double * parameter, size_t nRow, size_t nCol, size_t nPara, int numBlocks)
{
    double * dev_partial;
    HANDLE_ERROR(cudaMalloc((void**)&dev_partial,sizeof(double)*numBlocks));
    cudaDeviceSynchronize();

    reduceFunc<numThreads><<<numBlocks, numThreads, numThreads*sizeof(double)>>>(
            dev_partial,in, parameter, nRow,nCol, nPara);
////    cudaDeviceSynchronize();
////    printFirstElement<<<1,1>>>(dev_partial);
    cudaDeviceSynchronize();
    reduceSum<256><<<1, 256,256*sizeof(double)>>>(
            answer, dev_partial, numBlocks);
    HANDLE_ERROR(cudaFree(dev_partial));
}

void reduceFunc(double *out, const double *in, const double *parameter,  size_t nRow,
                  size_t nCol, size_t nPara, int numBlocks, int numThreads )
{
    switch ( numThreads ) {
        case 1:
            return reduceFunc_Template<1>(out, in, parameter, nRow, nCol, nPara, numBlocks);
        case 2:
            return reduceFunc_Template<2>(out, in, parameter, nRow, nCol, nPara, numBlocks);
        case 4:
            return reduceFunc_Template<4>(out, in, parameter, nRow, nCol, nPara, numBlocks);
        case 8:
            return reduceFunc_Template<8>(out, in, parameter, nRow, nCol, nPara, numBlocks);
        case 16:
            return reduceFunc_Template<16>(out, in, parameter, nRow, nCol, nPara, numBlocks);
        case 32:
            return reduceFunc_Template<32>(out, in, parameter, nRow, nCol, nPara, numBlocks);
        case 64:
            return reduceFunc_Template<64>(out, in, parameter, nRow, nCol, nPara, numBlocks);
        case 128:
            return reduceFunc_Template<128>(out, in, parameter, nRow, nCol, nPara, numBlocks);
        case 256:
            return reduceFunc_Template<256>(out, in, parameter, nRow, nCol, nPara, numBlocks);
        case 512:
            return reduceFunc_Template<512>(out, in, parameter, nRow, nCol, nPara, numBlocks);
        case 1024:
            return reduceFunc_Template<1024>(out, in, parameter, nRow, nCol, nPara, numBlocks);
    }
}
