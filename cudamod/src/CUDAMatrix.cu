//
// Created by user on 7-8-16.
//

#include "CUDAMatrix.cuh"
#include "cuda_runtime.h"
#include "Macro.cuh"

template<typename T, typename isRowMajor, typename ViewOrNor>
CUDA_HOST CUDAMatrix<T,isRowMajor,ViewOrNor>::CUDAMatrix(size_t Row, size_t Col,T* data ):nRow(Row),nCol(Col), hostData(Row, Col, data){
    cudaMalloc((void**)&devData, sizeof(T)*nCol*nRow);
}

template<typename T, typename isRowMajor, typename ViewOrNor>
CUDA_HOST CUDAMatrix<T, isRowMajor, ViewOrNor>::~CUDAMatrix() {
    cudaFree(devData);
}

template<typename T, typename isRowMajor, typename ViewOrNor>
CUDA_HOST void CUDAMatrix<T, isRowMajor, ViewOrNor>::syncDeviceToHost() {
    cudaMemcpy(devData,hostData.data,sizeof(T)*nRow*nCol, cudaMemcpyDeviceToHost);
}

template<typename T, typename isRowMajor, typename ViewOrNor>
CUDA_HOST void CUDAMatrix<T, isRowMajor, ViewOrNor>::syncHostToDevice() {
    cudaMemcpy(hostData.data,devData,sizeof(T)*nRow*nCol, cudaMemcpyHostToDevice);
}


template class CUDAMatrix<double, RowMajor, NotView>;
template class CUDAMatrix<double, RowMajor,View>;