//
// Created by user on 21-7-16.
//

#include <gtest/gtest.h>
#include <memory>
#include <cstdio>
#include <cstdlib>
#include <iostream>



class CudaTest1 : public ::testing::TestWithParam<int> {
public:

};

TEST_F(CudaTest1, TestForSummation){

    ASSERT_NEAR(1,1,0.1);
}

INSTANTIATE_TEST_CASE_P(Instantiation, CudaTest1, ::testing::Range(1, 1000));