//
// Created by user on 26-7-16.
//

#include <assert.h>
#include <iostream>
#include <functional>
#include "PointerMatrixHelper.h"
#include "PointerMatrix.h"

template<typename T>
void PointerMatrixHelper<T>::reshape(T *data, size_t nRow, size_t nCol, int alignment) {
    assert(nRow%alignment==0);
    PointerMatrixViewRowMajor<T> matrixView(nRow, nCol,data); //We have assumed that the data is row major
    PointerMatrixRowMajor<T> matrixTemp(alignment, nCol);
    for (unsigned long k = 0; k<nRow/alignment;k++) {
        for (int i = 0; i < alignment; i++) {
            for (int j = 0; j < nCol; j++) {
                matrixTemp(i,j)=matrixView(k*alignment+i, j);
            }
        }
        for (int i = 0; i < alignment; i++) {
            for (int j = 0; j < nCol; j++) {
                data[k*nCol+i+j*alignment] = matrixTemp(i,j);
            }
        }
    }

}


template class PointerMatrixHelper<double>;
template class PointerMatrixHelper<float>;
template class PointerMatrixHelper<int>;
template class PointerMatrixHelper<long>;

