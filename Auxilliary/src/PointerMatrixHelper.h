//
// Created by user on 26-7-16.
//

#ifndef PSTAT_POINTERMATRIXHELPER_H
#define PSTAT_POINTERMATRIXHELPER_H
#include <cstdio>
#include <cstdlib>

#include "PointerMatrix.h"

template<typename T>
class PointerMatrixHelper {
//    This methods prepare the alignment of data so that SIMD can be used
public:
    static void reshape(T* data, size_t nRow, size_t nCol, int alignment);
    template<typename isRowMajor>
    static void print(PointerMatrix<T, isRowMajor>& matrix){
        size_t nRow = matrix.getNRow();
        size_t nCol = matrix.getNCol();

        for (unsigned long i =0 ;i < nRow; i++){
            std::cout << matrix(i,0);
            for (unsigned long j =1; j<nCol;j++ ){
                std::cout << "," << matrix(i,j);
            }
            std::cout << std::endl;
        }
    }
    template<typename isRowMajor>
    static void print(PointerMatrixView<T, isRowMajor>& view){
        size_t nRow = view.getNRow();
        size_t nCol = view.getNCol();

        for (unsigned long i =0 ;i < nRow; i++){
            std::cout << view(i,0);
            for (unsigned long j =1; j<nCol;j++ ){
                std::cout << "," << view(i,j);
            }
            std::cout << std::endl;
        }
    }
};


#endif //PSAT_POINTERMATRIXHelper_H
