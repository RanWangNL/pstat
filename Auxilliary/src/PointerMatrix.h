//
// Created by user on 26-7-16.
//

#ifndef PSTAT_POINTERMATRIX_H
#define PSTAT_POINTERMATRIX_H

#include <stddef.h>
class PointerMatrixBase {
public:
    size_t nRow;
    size_t nCol;
    PointerMatrixBase(size_t nRow_, size_t nCol_);
    inline size_t getNRow(){
        return nRow;
    }
    inline size_t getNCol(){
        return nCol;
    }
};

template<typename T, typename isRowMajor>
class PointerMatrix : public PointerMatrixBase{
public:
    T * data;
    PointerMatrix(size_t nRow_, size_t nCol, T* DoNotUSE = nullptr);
    ~PointerMatrix();
    inline T& operator()(size_t Row, size_t Col){
        return static_cast<isRowMajor*>(this)->implementation(Row, Col);
    }
};

template<typename T, typename isRowMajor>
class PointerMatrixView : public PointerMatrixBase{
public:
    T * data;
    PointerMatrixView( size_t nRow_, size_t nCol, T * data_);
    inline T& operator()(size_t Row, size_t Col){
        return static_cast<isRowMajor*>(this)->implementation(Row,Col);

    }
};

template<typename T>
class PointerMatrixRowMajor: public PointerMatrix<T, PointerMatrixRowMajor<T>>{
public:
    PointerMatrixRowMajor(size_t nRow_, size_t nCol_, T* data = nullptr):PointerMatrix<T, PointerMatrixRowMajor<T>>(nRow_, nCol_,data) { }
    inline T& implementation(size_t Row, size_t Col){
        using super = PointerMatrix<T, PointerMatrixRowMajor<T>>;
        return super::data[Row*super::getNCol()+Col];
    }
};

template<typename T>
class PointerMatrixColMajor: public PointerMatrix<T, PointerMatrixColMajor<T>>{
public:
    PointerMatrixColMajor(size_t nRow_, size_t nCol_, T* data =nullptr):PointerMatrix<T, PointerMatrixColMajor<T>>(nRow_,nCol_, data) {}
    inline T& implementation(size_t Row, size_t Col){
        using super= PointerMatrix<T, PointerMatrixColMajor<T>>;
        return super::data[Row+Col*super::getNRow()];
    }
};

template<typename T>
class PointerMatrixViewRowMajor :public PointerMatrixView<T, PointerMatrixViewRowMajor<T>>{
public:
    PointerMatrixViewRowMajor(size_t nRow_, size_t nCol_, T* data);

    inline T& implementation(size_t Row, size_t Col){
        using super= PointerMatrixView<T, PointerMatrixViewRowMajor<T>>;
        return super::data[Row*super::getNCol()+Col];
    }
};

template<typename T>
class PointerMatrixViewColMajor : public PointerMatrixView<T, PointerMatrixColMajor<T>>{
public :
    PointerMatrixViewColMajor(size_t nRow_, size_t nCol_, T* data);
    inline T& implementation(size_t Row, size_t Col){
        using super = PointerMatrixView<T, PointerMatrixViewColMajor<T>>;
        return super::data[Row+Col*super::getNRow()];
    }

};
#endif //PSTAT_POINTERMATRIX_H
