//
// Created by user on 6-8-16.
//

#ifndef PSTAT_NORMALRANDOM_H
#define PSTAT_NORMALRANDOM_H


#include <boost/random.hpp>
#include <cstdio>
#include <cstdlib>
template<typename T>
class NormalRandom {
private:
    typedef boost::random::mt19937_64 generator;
    typedef boost::random::normal_distribution<>  distribution;
    typedef boost::variate_generator<generator&, distribution > gen;
    generator myg;
    distribution mydist;
    gen mygen;

    double mean;
    double std;
public:
    void setMean(double mean) {
        NormalRandom::mean = mean;
    }

    void setSTD(double std) {
        NormalRandom::std = std;
    }


    double getMean() const {
        return mean;
    }

    double getStd() const {
        return std;
    }

    NormalRandom(int seed, double mean_, double variance_): myg(seed), mydist(0.0,1.0), mygen(myg, mydist), mean(mean_),
                                                            std(variance_){}
//    Fill an array of normal random variables
//    The random variables are assumed to be i.i.d

    template<typename arg, typename...args>
    void fill(arg& a, args&...b){
        fill(a);
        fill(b...);
    };
    template<typename arg>
    void fill(arg& a){
        unsigned long int nRow = a.getNRow();
        unsigned long int nCol = a.getNCol();
        for (unsigned long int i =0 ;i < nRow; i++){
            for (unsigned long int j=0;j<nCol;j++){
                a(i,j)=std*mygen()+mean;
            }
        }
    }

};



#endif //PSTAT_NORMALRANDOM_H
