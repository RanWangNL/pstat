#include "PointerMatrix.h"
#include <cstdio>
#include <cstdlib>

PointerMatrixBase::PointerMatrixBase(size_t nRow_, size_t nCol_):nRow{nRow_}, nCol{nCol_} { }



template<typename T, typename isRowMajor>
PointerMatrix<T, isRowMajor>::PointerMatrix(size_t nRow_, size_t nCol_,T* DONOTUSE):PointerMatrixBase(nRow_, nCol_) {
    data = (T* ) malloc(sizeof(T)*nRow*nCol);
}
template<typename T, typename isRowMajor>
PointerMatrix<T, isRowMajor>::~PointerMatrix() {
    free(data);
}

template<typename T, typename isRowMajor>
PointerMatrixView<T, isRowMajor>::PointerMatrixView( size_t nRow_, size_t nCol_,T *data_):PointerMatrixBase(nRow_, nCol_) {
    data = data_;
}



template<typename T>
PointerMatrixViewRowMajor<T>::PointerMatrixViewRowMajor( size_t nRow_, size_t nCol_,T *data_):PointerMatrixView<T, PointerMatrixViewRowMajor<T>>(nRow_, nCol_,data_ ) { }

template<typename T>
PointerMatrixViewColMajor<T>::PointerMatrixViewColMajor( size_t nRow_, size_t nCol_, T *data_): PointerMatrixView<T, PointerMatrixViewColMajor<T>>(nRow_, nCol_, data_){ }


template class PointerMatrix<double, PointerMatrixRowMajor<double>>;
template class PointerMatrix<float, PointerMatrixRowMajor<float>>;
template class PointerMatrix<long, PointerMatrixRowMajor<long>>;
template class PointerMatrix<int, PointerMatrixRowMajor<int>>;

template class PointerMatrixColMajor<double>;
template class PointerMatrixRowMajor<double>;
template class PointerMatrixColMajor<int>;
template class PointerMatrixRowMajor<int>;

template class PointerMatrixViewRowMajor<double>;
template class PointerMatrixViewRowMajor<float>;
template class PointerMatrixViewRowMajor<int>;
template class PointerMatrixViewRowMajor<long>;


