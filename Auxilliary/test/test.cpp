//
// Created by user on 6-8-16.
//

#include <gtest/gtest.h>
#include <memory>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "../src/PointerMatrix.h"
#include "../src/NormalRandom.hpp"
#include "../src/PointerMatrixHelper.h"
class PointerMatrixTest : public ::testing::TestWithParam<int> {
public:

//    double * data;
//    long nRow;
//    long nCol;
//    threadpool pool;
//    ThreadPoolTest1(): nRow{1000}, nCol{10}, pool{1}{
//        data =(double *) malloc(sizeof(double)*nRow*nCol);
//        NormalRandom<double> randomfiller(1000);
//        randomfiller.fillRowMajor(data, nRow, nCol, 0.0, 1.0);
//
//    }
    PointerMatrixRowMajor<double> matrix;
    NormalRandom<double> random;
    PointerMatrixTest():matrix(12,10), random(1000,0,1){
        random.fill(matrix);
    }


    //void SetUp( ) {
    // code here will execute just before the test ensues
    //}

    //void TearDown( ) {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
    //}

//    ~ThreadPoolTest1( )  {
//        free(data);
//    }

    // put in any custom data members that you need

};

TEST_F(PointerMatrixTest, TestForPrint){
//    double parameter = 0.0;
//    double sumFromCPU =0.0;
//    for (int i =0 ;i< nRow;i++){
//        sumFromCPU += data[i*nCol];
//    }
//    double sumFromThreadPool = accumulateLikelihood(pool, data, nRow, nCol, &parameter, 1, 200);
    PointerMatrixRowMajor<double> matrix(10,10);
    NormalRandom<double> random(1000,0,1);
    random.fill(matrix);
    PointerMatrixHelper<double>::print(matrix);
    ASSERT_EQ(1,1);
}

TEST_F(PointerMatrixTest, ReshapeTest){
    std::cout << "Before Reshape" << std::endl;
    PointerMatrixHelper<double>::print(matrix);
    PointerMatrixHelper<double>::reshape(matrix.data, matrix.getNRow(), matrix.getNCol(), 4);
    std::cout << "After Reshape" << std::endl;
    PointerMatrixHelper<double>::print(matrix);
}
INSTANTIATE_TEST_CASE_P(Instantiation, PointerMatrixTest, ::testing::Range(1, 1000));